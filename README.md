# AUS_WA_GeographeBay
Western Australia, Geographe Bay

## Roadmap
- visual: multi-D map of Geo Bay topography
- cartography: list reef transects for practical application (fishing, diving, observing)

## Files
- Bathymetry.Rmd - investigation
- IW processes Black Sea_function share.Rmd - useful data techniques
- Wavelet_Interpol_funtion share.Rmd - useful data techniques
- /data - shapefile with bathymetry data for investigation
- marmap_coord.csv - cannot remember connection

## Description
Integrated Marine Observing System (IMOS) provides Free data for Australian marine environments, collating data from..
IMAS Institute for Marine and Antarctic Studies (IMAS), through University of Tasmania, and funded through..
The Marine Futures Project, designed to benchmark the current status of key Western Australian marine ecosystems, based on an improved understanding of the relationship between marine habitats, biodiversity and our use of these values. 

Approximately 1,500 km2 of seafloor were mapped using hydroacoustics (Reson 8101 Multibeam), and expected benthic habitats "ground-truthed" using towed video transects and baited remote underwater video systems. Both sources of information were then combined in a spatial predictive modelling framework to produce fine-scale habitat maps showing the extent of substrate types, biotic formations, etc.

Surveys took place across 9 study areas, including Geographe Bay in the southwest Capes region. The marine environment at this location varies from extensive seagrass meadows in protected waters, to kelp-dominated granite and limestone reefs in areas of high wave energy. A small number of corals are also found throughout the region, reflecting the influence of the southward flow of the Leeuwin Current. The fish fauna is also diverse, with a high proportion of endemic species.
Lineage
Areas of seafloor in water deeper than 10 metres were surveyed with hydroacoustics using a Reson 8101 Multibeam or interferometric swath echosounder system, mounted on the hull of the sampling vessel. These data were processed to construct full coverage maps of seafloor bathymetry and textural information. These maps, combined with observations recorded from in situ video footage, unerpinned the development of statistical models that produced the most efficient, objective, and ecologically meaningful classifications of sea floor features and inhabitants as possible for natural resource management and planning. 

## Source / Resources
https://imos.org.au/

https://www.imas.utas.edu.au/

https://metadata.imas.utas.edu.au/geonetwork/srv/eng/catalog.search#/metadata/275f62b0-0b6c-4514-90c7-deeae423ab23
Meeuwig, Jessica ; Radford, Ben (2016): Marine Futures Project - Geographe Bay - reef habitat. .dataset.
http://catalogue-aodn.prod.aodn.org.au/geonetwork/srv/eng/search?uuid=275f62b0-0b6c-4514-90c7-deeae423ab23

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Contributing
I am 'cleaning up' all my projects/files towards publishing a paper, allowing me to apply for further studies and grants etc.. all help is appreciated. 

## Authors and acknowledgment
Lets work together!

## License
Private invitation collaborations towards publishing, but always open to sharing techniques/code.

## Project status
Ongoing
